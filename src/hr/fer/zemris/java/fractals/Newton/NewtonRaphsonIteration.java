package hr.fer.zemris.java.fractals.Newton;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;

import hr.fer.zemris.java.fractals.complex.Complex;
import hr.fer.zemris.java.fractals.complex.ComplexRootedPolynomial;
import hr.fer.zemris.java.fractals.viewer.FractalViewer;
import hr.fer.zemris.java.fractals.viewer.IFractalProducer;
import hr.fer.zemris.java.fractals.viewer.IFractalResultObserver;

/**
 * NewtonRaphsonIteration is program that illustrates Newton-Raphson's iterative method for
 * finding the roots of some equation. This program allows user to enter only polynomial
 * equations: C -> C. Meaning that both domain and codomain are in complex set. This program
 * expects no arguments via command line, all arguments must be given via standard input,
 * System.in. Given arguments must be complex numbers that represent polynomial roots.
 * This program will recreate iterations of Newton-Raphson's method and return its results
 * as picture in color.
 * 
 * @author Josip Kasap
 *
 */
public class NewtonRaphsonIteration {

	/**
	 * Main method for executing NewtonRaphsonIteration program.
	 * @param args command-line arguments
	 */
	public static void main(String[] args) {
		
		if (args.length != 0) {
			System.err.println("This program should not get any command-line arguments.");
			System.exit(0);
		}
		
		System.out.println("Welcome to Newton-Raphson iteration-based fractal viewer.");
		System.out.println("Please enter at least two roots, one root per line. Enter 'done' when done.");
		
		int rootNumber = 1;
		List<Complex> roots = new ArrayList<>();
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
			while (true) {
				System.out.print("Root " + rootNumber++ + "> ");
				String line = reader.readLine();
				if (line == null) {
					System.err.println("IO-stream failed. Terinating program.");
					System.exit(-1);
				}
				line = line.trim();
				
				if (line.isEmpty()) continue;
				if (line.equalsIgnoreCase("done")) break;
				
				roots.add(Complex.parse(line));
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		} catch (NumberFormatException e) {
			System.err.println(e.getMessage());
			System.exit(0);
		}
		
		System.out.println("Image of fractal will appear shortly. Thank you.");
		FractalViewer.show(new MyProducer(roots.toArray(new Complex[roots.size()])));
	}
	
	/**
	 * CalculationJob is implementation of {@link Callable} which is used to calculate
	 * results and display them.
	 * 
	 * @author Josip Kasap
	 *
	 */
	public static class CalculationJob implements Callable<Void> {

		/** Minimal real part of complex number. */
		private double reMin;
		
		/** Maximal real part of complex number. */
		private double reMax;
		
		/** Minimal imaginary part of complex number. */
		private double imMin;
		
		/** Maximal imaginary part of complex number. */
		private double imMax;
		
		/** 
		 * Minimal difference between iterations after which is considered that
		 * iteration is completed. 
		 */
		private double convergenceThreshold;
		
		/** 
		 * Minimal difference between current iteration and closest root, if 
		 * current iteration is distanced less than this amount iteration will be
		 * completed. 
		 */
		private double rootThreshold;
		
		/** Width of screen. */
		private int width;
		
		/** Height of screen. */
		private int height;
		
		/** Minimal y value. */
		private int yMin;
		
		/** Maximal y value. */
		private int yMax;
		
		/** Maximal number of iterations. */
		private int maxIter;
		
		/** Result field. */
		private short[] data;
		
		/** Polynomial which roots are being calculated. */
		private ComplexRootedPolynomial polynom;

		/**
		 * Constructs new CalculationJob with given parameters.
		 * @param reMin minimal real part of complex number
		 * @param reMax maximal real part of complex number
		 * @param imMin minimal imaginary part of complex number
		 * @param imMax maximal imaginary part of complex number
		 * @param width width of screen
		 * @param height height of screen
		 * @param yMin minimal y value
		 * @param yMax maximal y value
		 * @param polynom polynomial which roots are being calculated
		 * @param convergenceThreshold minimal difference between iterations
		 * @param rootThreshold minimal difference between current iteration 
		 * 				and closest root
		 * @param maxIter maximal number of iterations
		 * @param data result field
		 */
		public CalculationJob(double reMin, double reMax, double imMin,
				double imMax, int width, int height, int yMin, int yMax, 
				ComplexRootedPolynomial polynom, double convergenceThreshold, 
				double rootThreshold, int maxIter, short[] data) {
			
			super();
			this.reMin = reMin;
			this.reMax = reMax;
			this.imMin = imMin;
			this.imMax = imMax;
			this.yMin = yMin;
			this.yMax = yMax;
			this.width = width;
			this.height = height;
			this.polynom = polynom;
			this.data = data;
			this.convergenceThreshold = convergenceThreshold;
			this.rootThreshold = rootThreshold;
			this.maxIter = maxIter;
		}
		
		@Override
		public Void call() {
			NewtonRaphsonMethod.calculate(
					reMin, reMax, imMin, imMax, width, height,
					yMin, yMax, rootThreshold, convergenceThreshold, polynom, maxIter, data
			);
			return null;
		}
	}
	
	/**
	 * Implementation of {@link IFractalProducer} which calculates
	 * and displays results of Newton-Raphson's iterative method for
	 * given polynomial.
	 * 
	 * @author Josip Kasap
	 *
	 */
	public static class MyProducer implements IFractalProducer {
		
		/** Default value for maximal iteration count. */
		public static final int MAX_ITERATIONS = 16*16*16;
		
		/** Default value for minimal distance between iteration and closest root. */
		public static final double CONVERGENCE_THRESHOLD = 0.001;
		
		/** Default value for minimal distance between two iterations. */
		public static final double ROOT_THRESHOLD = 0.002;
		
		/** ThreadPool for producing results. */
		private ExecutorService pool;
		
		/** Maximal iteration count. */
		private int maxIter;
		
		/** Minimal distance between iteration and closest root. */
		private double convergenceThreshold;
		
		/** Minimal distance between two iterations. */
		private double rootThreshold;
		
		/** Polynomial. */
		private ComplexRootedPolynomial polynomial;
		
		/**
		 * Constructs new producer from given roots of polynomial.
		 * @param roots roots of polynomial
		 */
		public MyProducer(Complex[] roots) {
			polynomial = new ComplexRootedPolynomial(roots);
			pool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors(),
					new DaemonicThreadFactory());
			
			maxIter = MAX_ITERATIONS;
			convergenceThreshold = CONVERGENCE_THRESHOLD;
			rootThreshold = ROOT_THRESHOLD;
		}

		@Override
		public void produce(double reMin, double reMax, double imMin, double imMax,
				int width, int height, long requestNo, IFractalResultObserver observer) {
			
			short[] data = new short[width * height];
			final int tapeNumber = Runtime.getRuntime().availableProcessors() * 8;
			int numOfYPerTape = height / tapeNumber;
			List<Future<Void>> results = new ArrayList<>();
			
			for(int i = 0; i < tapeNumber; i++) {
				int yMin = i*numOfYPerTape;
				int yMax = (i + 1)*numOfYPerTape - 1;
				if(i == tapeNumber - 1) {
					yMax = height - 1;
				}
				
				CalculationJob job = new CalculationJob(reMin, reMax, imMin, imMax, width, height,
						yMin, yMax, polynomial, convergenceThreshold, rootThreshold, maxIter, data);
				
				results.add(pool.submit(job));
			}
			
			for(Future<Void> job : results) {
				try {
					job.get();
				} catch (InterruptedException | ExecutionException e) {
				}
			}
			
			observer.acceptResult(data, (short)(polynomial.toComplexPolynom().order() + 1), requestNo);
		}
		
	}
	
	/**
	 * Implementation of {@link ThreadFactory} which creates daemon threads.
	 * 
	 * @author Josip Kasap
	 *
	 */
	private static class DaemonicThreadFactory implements ThreadFactory {
		@Override
		public Thread newThread(Runnable job) {
			Thread deamonThread = new Thread(job);
			deamonThread.setDaemon(true);
			return deamonThread;
		}
		
	}
}
