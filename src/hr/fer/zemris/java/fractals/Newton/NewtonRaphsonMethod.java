package hr.fer.zemris.java.fractals.Newton;

import hr.fer.zemris.java.fractals.complex.Complex;
import hr.fer.zemris.java.fractals.complex.ComplexPolynomial;
import hr.fer.zemris.java.fractals.complex.ComplexRootedPolynomial;

/**
 * NewtonRaphsonMethod is class which calculates iterations of Newton-Raphson's
 * iterative method. This class has single public static method: {@link #calculate}
 * which calculates results of iterations and stores it in given data array.
 * 
 * @author Josip Kasap
 *
 */
public class NewtonRaphsonMethod {

	/**
	 * This method calculates results of Newton-Raphson's iterative method and stores
	 * result in data array.
	 * @param reMin minimal real part of complex number
	 * @param reMax maximal real part of complex number
	 * @param imMin minimal imaginary part of complex number
	 * @param imMax maximal imaginary part of complex number
	 * @param width width of screen
	 * @param height height of screen
	 * @param yMin minimal y value
	 * @param yMax maximal y value
	 * @param rootThreshold minimal difference between iteration and closest root
	 * @param convergenceThreshold minimal difference between iterations
	 * @param rootedPolynomial polynomial used in Newton-Raphson's method
	 * @param maxIter maximal number of iterations
	 * @param data result field
	 */
	public static void calculate(double reMin, double reMax, double imMin, double imMax, 
			int width, int height, int yMin, int yMax, double rootThreshold, 
			double convergenceThreshold, ComplexRootedPolynomial rootedPolynomial, int maxIter, short[] data){
		
		ComplexPolynomial polynomial = rootedPolynomial.toComplexPolynom();
		ComplexPolynomial derivedPolynomial = polynomial.derive();
		
		int offset = yMin * width;
		for(int y = yMin; y <= yMax; y++) {
			for(int x = 0; x < width; x++) {
				
				Complex zn = mapToComplexPlain(x, y, width, height, reMin, reMax, imMin, imMax);
				Complex zn1;
				int iteration = 0;
				double module;
				
				do {
					Complex numerator = polynomial.apply(zn);
					Complex denominator = derivedPolynomial.apply(zn);
					Complex fraction = numerator.divide(denominator);
					zn1 = zn.sub(fraction);
					module = zn1.sub(zn).module();
					zn = zn1;
					iteration++;
				} while(module > convergenceThreshold && iteration < maxIter);
				
				int index = rootedPolynomial.indexOfClosestRootFor(zn1, rootThreshold);
				if(index != -1) {
					data[offset++] = (short)index; 
				} else { 
					data[offset++] = 0; 
				}
			}
		}
	}
	
	/**
	 * Maps given pixel to complex plain.
	 * @param x x position of pixel
	 * @param y y position of pixel
	 * @param width number of pixels in row
	 * @param height number of pixels in column
	 * @param reMin minimal real part of complex number
	 * @param reMax maximal real part of complex number
	 * @param imMin minimal imaginary part of complex number
	 * @param imMax maximal imaginary part of complex number
	 * @return complex number that represents given pixel
	 */
	private static Complex mapToComplexPlain(double x, double y, int width, int height, double reMin,
			double reMax, double imMin, double imMax){
	    double real = x / (width - 1.0) * (reMax - reMin) + reMin;
		double imaginary = (height - 1.0 - y) / (height - 1) * (imMax - imMin) + imMin;
	    return new Complex(real, imaginary);
	}
}
