package hr.fer.zemris.java.fractals.complex;

import java.util.ArrayList;
import java.util.List;

/**
 * Class Complex is representation of complex number which implements usual
 * methods for complex numbers, such as: addition, multiplication, module...
 * Complex is immutable complex number, meaning once it is created it is impossible
 * to change its value.
 * 
 * @author Josip Kasap
 *
 */
public class Complex {

	/** Real part of complex number. */
	private final double real;
	
	/** imaginary part of complex number. */
	private final double imaginary;
	
	/** 0 + 0i */
	public static final Complex ZERO = new Complex(0,0);
	
	/** 1 + 0i */
	public static final Complex ONE = new Complex(1,0);
	
	/** -1 + 0i */
	public static final Complex ONE_NEG = new Complex(-1,0);
	
	/** 0 + 1i */
	public static final Complex IM = new Complex(0,1);
	
	/** 0 - 1i */
	public static final Complex IM_NEG = new Complex(0,-1);
	
	/**
	 * Constructs new Complex with given real and imaginary part.
	 * @param real real part of complex number
	 * @param imaginary imaginary part of complex number
	 */
	public Complex(double real, double imaginary) {
		super();
		this.real = real;
		this.imaginary = imaginary;
	}
	
	/**
	 * Constructs new 0 + 0i complex number (zero).
	 */
	public Complex() {
		this(0,0);
	}
	
	/**
	 * Returns module of this complex number.
	 * @return module of this complex number
	 */
	public double module() {
		return Math.sqrt(real*real + imaginary*imaginary);
	}
	
	/**
	 * Multiples this complex number with other
	 * @param c other complex number
	 * @return result of multiplication
	 * @throws IllegalArgumentException if other complex is null value
	 */
	public Complex multiply(Complex c) {
		if (c == null) {
			throw new IllegalArgumentException("Null is not valid argument for this method.");
		}
		return new Complex(real*c.real - imaginary*c.imaginary, real*c.imaginary + imaginary*c.real);
	}
	
	/**
	 * Divides this complex number with other
	 * @param c other complex number
	 * @return result of division
	 * @throws IllegalArgumentException if other complex is null value
	 * @throws ArithmeticException if other complex number is zero (0 + 0i)
	 */
	public Complex divide(Complex c) {
		if (c == null) {
			throw new IllegalArgumentException("Null is not valid argument for this method.");
		}
		if (c.equals(ZERO)) {
			throw new ArithmeticException("Dividing with 0 is not possible operation.");
		}
		
		double cMod2 = c.real*c.real + c.imaginary*c.imaginary;
		double newReal = (real*c.real + imaginary*c.imaginary)/cMod2;
		double newImaginary = (-real*c.imaginary + imaginary*c.real)/cMod2;
		return new Complex(newReal, newImaginary);
	}
	
	/**
	 * Adds other complex number onto this
	 * @param c other complex number
	 * @return result of addition
	 * @throws IllegalArgumentException if other complex is null value
	 */
	public Complex add(Complex c) {
		if (c == null) {
			throw new IllegalArgumentException("Null is not valid argument for this method.");
		}
		return new Complex(real+c.real, imaginary+c.imaginary);
	}
	
	/**
	 * Subducts other complex number onto this
	 * @param c other complex number
	 * @return result of subduction
	 * @throws IllegalArgumentException if other complex is null value
	 */
	public Complex sub(Complex c) {
		if (c == null) {
			throw new IllegalArgumentException("Null is not valid argument for this method.");
		}
		return new Complex(real-c.real, imaginary-c.imaginary);
	}
	
	/**
	 * Negates this complex number
	 * @return negated complex number
	 */
	public Complex negate() {
		return new Complex(-real, -imaginary);
	}
	
	/**
	 * Powers this complex number to given exponent
	 * @param n exponent of this complex number
	 * @return new complex number that represents result of powering
	 * @throws ArithmeticException if n is less than 0
	 */
	public Complex power(int n) {
		if (n < 0) {
			throw new ArithmeticException("Power of complex number must not be negative number.");
		}
		
		if (this.equals(ZERO)) {
			return ZERO;
		}
		
		double fi = angle();
		double r = module();
		
		return fromModuleAndAngle(Math.pow(r, n), n*fi);
	}
	
	/**
	 * Returns angle of this number.
	 * @return angle of this number
	 */
	public double angle() {
		if (this.equals(ZERO)) return 0;
		double fi = Math.atan2(imaginary, real);
		return fi > 0 ? fi : 2*Math.PI+fi;
	}
	
	/**
	 * Returns new complex number from given module and angle.
	 * @param module module of complex number
	 * @param angle angle of complex number
	 * @return new complex number from given module and angle
	 * @throws IllegalArgumentException if module is less than 0
	 */
	public static Complex fromModuleAndAngle(double module, double angle) {
		if (module < 0) {
			throw new IllegalArgumentException("Module must be positive number.");
		}
		return new Complex(module*Math.cos(angle), module*Math.sin(angle));
	}
	
	/**
	 * Calculates and returns all roots of given complex number.
	 * @param n n<sub>th</sub> root of number
	 * @return list of all roots of given complex number
	 * @throws IllegalArgumentException if n is less or equal to 0
	 */
	public List<Complex> root(int n) {
		if (n <= 0) {
			throw new IllegalArgumentException("Root of must be greater than 0.");
		}
		
		List<Complex> roots = new ArrayList<>();
		double r = module();
		double fi = (this.equals(ZERO) ? 0 : angle());
		
		for (int i=0; i<n; i++) {
			roots.add(fromModuleAndAngle(Math.pow(r, 1.0/n), (fi + 2*i*Math.PI)/n));
		}
		return roots;
	}
	
	/**
	 * Parses given string and returns complex number that this string represents.
	 * @param s complex number in string format
	 * @return new complex number with given string representation
	 * @throws IllegalArgumentException if string is null value
	 * @throws NumberFormatException if this string does not represent complex number
	 */
	public static Complex parse(String s) {
		if (s == null) {
			throw new IllegalArgumentException("Null is not valid argument for this method.");
		}
		s = s.replaceAll("\\s", "");
		
		int signForFirst;
		if (s.startsWith("-")) {
			s = s.substring(1);
			signForFirst = -1;
		} else {
			signForFirst = 1;
		}
		
		String[] arguments = s.split("[\\Q+-\\E]");
		if (arguments.length == 2) {
			if (!arguments[1].startsWith("i")) {
				throw new NumberFormatException("Given String does not represent complex number.");
			}
			
			arguments[1] = arguments[1].substring(1);
			if (arguments[1].isEmpty()) {
				arguments[1] = "1";
			}
			
			try {
				//ako je postojao - na početku, makli smo ga, i promjenili predznak realnom broju.
				double real = Double.parseDouble(arguments[0]) * signForFirst; 
				
				//ako su realni i imaginarni razdjeljeni sa -, makli smo minus, i moramo negirati imaginarni dio.
				double imaginary = Double.parseDouble(arguments[1]) * (s.contains("-") ? -1 : 1);
				
				return new Complex(real, imaginary);
			} catch (NumberFormatException ignorable) {
				throw new NumberFormatException("Given String does not represent complex number.");
			}
		} else if (arguments.length > 2 || arguments.length == 0) {
			throw new NumberFormatException("Given String does not represent complex number.");
		}
		
		if (s.startsWith("i")) {
			s = s.substring(1);
			if (s.isEmpty()) {
				s = "1";
			}
			try {
				return new Complex(0, Double.parseDouble(s) * signForFirst);
			} catch (NumberFormatException ignorable) {
				throw new NumberFormatException("Given String does not represent complex number.");
			}
		}
		
		try {
			return new Complex(Double.parseDouble(s) * signForFirst, 0);
		} catch (NumberFormatException ignorable) {
			throw new NumberFormatException("Given String does not represent complex number.");
		}
	}
	
	@Override
	public String toString() {
		if (imaginary > 0) {
			return String.format("%.4f+i%.4f", real, imaginary);
		} else {
			return String.format("%.4f-i%.4f", real, Math.abs(imaginary));
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(imaginary);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(real);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Complex other = (Complex) obj;
		if (!compareLongs(Double.doubleToLongBits(imaginary), Double.doubleToLongBits(other.imaginary)))
			return false;
		if (!compareLongs(Double.doubleToLongBits(real), Double.doubleToLongBits(other.real)))
			return false;
		return true;
	}
	
	/**
	 * Compares 2 long numbers that represent bits of some double value.
	 * @param arg0 first long number
	 * @param arg1 second long number
	 * @return <code>true</code> if 2 numbers represent same double value
	 * 				<code>false</code> otherwise
	 */
	private boolean compareLongs(long arg0, long arg1) {
		double darg0 = Double.longBitsToDouble(arg0);
		double darg1 = Double.longBitsToDouble(arg1);
		double distance = Math.abs(darg0 - darg1);
		if (distance < 1E-9) return true;
		return false;
	}
}
