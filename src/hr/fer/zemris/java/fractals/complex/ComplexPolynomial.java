package hr.fer.zemris.java.fractals.complex;

import java.util.ArrayList;
import java.util.List;

/**
 * ComplexPolynomial represent some polynomial with complex arguments and complex
 * roots. This class stores its polynomial in polynomial form, meaning that it is
 * not aware of roots of its polynomial.
 * 
 * @author Josip Kasap
 *
 */
public class ComplexPolynomial {
	
	/** 
	 * List of Complex factors of polynomial. First number represents
	 * factor of variable with highest power in polynomial, while last 
	 * factor represent constant part of polynomial.
	 */
	private List<Complex> polynome;
	
	/** Order of polynomial. */
	private short order;

	/**
	 * Creates and returns new polynomial with given value of its roots.
	 * @param roots roots of polynomial
	 * @return new polynomial with given roots
	 * @throws IllegalArgumentException if no roots are given
	 */
	public static ComplexPolynomial fromRoots(Complex ...roots) {
		if (roots == null || roots.length == 0) {
			throw new IllegalArgumentException("Polynomial must have at least 1 root solution.");
		}
		ComplexPolynomial[] polys = new ComplexPolynomial[roots.length];
        
        for(int i = 0; i < roots.length; i++){
            polys[i] = new ComplexPolynomial(Complex.ONE, roots[i].negate());
        }
        ComplexPolynomial result = new ComplexPolynomial(Complex.ONE);
        for(int i = 0; i < roots.length; i++){
            result = result.multiply(polys[i]);
        }
        return result;
	}
	
	/**
	 * Constructs new polynomial with given factors. Factors must represent
	 * polynomial values in this form: first factor must be factor of variable 
	 * with highest power.
	 * @param factors factors of this polynomial
	 * @throws IllegalArgumentException if there are no factors
	 */
	public ComplexPolynomial(Complex ...factors) {
		if (factors == null || factors.length == 0) {
			throw new IllegalArgumentException("Polynomial must have at least 1 facter.");
		}
		polynome = new ArrayList<>(factors.length);
		for (int i = 0; i < factors.length; i++) {
			polynome.add(factors[i]);
		}
		order = (short)(polynome.size() - 1);
	}
	
	/**
	 * Returns order of polynomial.
	 * @return order of polynomial
	 */
	public short order() {
		return order;
	}
	
	/**
	 * Multiplies this polynomial with other
	 * @param p other polynomial
	 * @return result of multiplication
	 */
	public ComplexPolynomial multiply(ComplexPolynomial p) {
		if(p == null){
			throw new IllegalArgumentException("Null is not valid polynomial.");
		}
		
		Complex[] newPolynome = new Complex[order + p.order + 1];
		
		for(int i = 0; i < newPolynome.length; i++){
			newPolynome[i] = Complex.ZERO;
		}
		
		for(int i = 0; i < order + 1; i++){
			for(int j = 0; j < p.order + 1; j++){
				newPolynome[i+j] = newPolynome[i+j].add(
						polynome.get(i).multiply(p.polynome.get(j))
				);
			}
		}
		
		return new ComplexPolynomial(newPolynome);
	}
	
	/**
	 * Derives this polynomial.
	 * @return result of derivation
	 */
	public ComplexPolynomial derive() {
		Complex[] newPolynome = new Complex[order];
		
		for (int i = 0; i < newPolynome.length; i++) {
			newPolynome[i] = polynome.get(i).multiply(new Complex(order-i, 0));
		}
		
		return new ComplexPolynomial(newPolynome);
	}
	
	/**
	 * Applies given number into this polynomial.
	 * @param z complex number that is to be applied into this polynomial
	 * @return complex number that represent result of substitution variable
	 * 			of polynomial with given complex number
	 * @throws IllegalArgumentException if complex number is null value
	 */
	public Complex apply(Complex z) {
		
		if (z == null) {
			throw new IllegalArgumentException("Null is not valid polynomial.");
		}
		
		Complex result = Complex.ZERO;
		
		for (int i = 0; i < polynome.size(); i++) {
			result = result.add(z.power(order-i).multiply(polynome.get(i)));
		}
		
		return result;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append(String.format("(%s)*z^%d", polynome.get(0), order));
		
		for (int i = 1; i <= order; i++) {
			if (polynome.get(i).equals(Complex.ZERO)) continue;
			
			sb.append(" + ");
			sb.append(String.format("(%s)*z^%d", polynome.get(i), order-i));
		}
		
		return sb.toString();
	}
}
