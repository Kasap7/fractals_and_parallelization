package hr.fer.zemris.java.fractals.complex;

import java.util.ArrayList;
import java.util.List;

/**
 * ComplexRootedPolynomial represent some polynomial with complex arguments and complex
 * roots. This class stores polynomial as list of its roots, meaning that this class
 * does knows both the factors of polynomial, and its root solutions.
 * 
 * @author Josip Kasap
 *
 */
public class ComplexRootedPolynomial {
	
	/** List of roots. */
	private List<Complex> polyRoots;

	/**
	 * Constructs new polynomial with given root solutions.
	 * @param roots roots of polynomial
	 * @throws IllegalArgumentException if there are no roots
	 */
	public ComplexRootedPolynomial(Complex ...roots) {
		super();
		if (roots == null || roots.length == 0) {
			throw new IllegalArgumentException("Polynomial must have at least 1 root solution.");
		}
		polyRoots = new ArrayList<>();
		for (Complex root : roots) {
			polyRoots.add(root);
		}
	}
	
	/**
	 * Applies given number into this polynomial.
	 * @param z complex number that is to be applied into this polynomial
	 * @return complex number that represent result of substitution variable
	 * 			of polynomial with given complex number
	 * @throws IllegalArgumentException if complex number is null value
	 */
	public Complex apply(Complex z) {
		if (z == null) {
			throw new IllegalArgumentException("Null is not valid argument for this method.");
		}
		
		Complex result = Complex.ONE;
		
		for (Complex root : polyRoots) {
			result = result.multiply(z.sub(root));
		}
		
		return result;
	}
	
	/**
	 * Returns {@link ComplexPolynomial} form of this polynomial.
	 * @return {@link ComplexPolynomial} form of this polynomial
	 */
	public ComplexPolynomial toComplexPolynom() {
		return ComplexPolynomial.fromRoots(polyRoots.toArray(new Complex[polyRoots.size()]));
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Complex root : polyRoots) {
			sb.append(String.format("(z-[%s])", root.toString()));
		}
		return sb.toString();
	}
	
	/**
	 * Returns index of root point that is closest to given complex number.
	 * If distance from closes root point to given complex number is higher
	 * than given threshold, this method will return -1.
	 * @param z complex number used to determine closest index of root of 
	 * 				this polynomial
	 * @param treshold maximal allowed distance
	 * @return index of root point that is closest to given complex number
	 * 		or -1 if that distance is higher than threshold
	 */
	public int indexOfClosestRootFor(Complex z, double treshold) {
		if (z == null) {
			throw new IllegalArgumentException("Null is not valid argument for this method.");
		}
		int result = -1;
		double minDistance = treshold;
		
		for (int i = 0; i < polyRoots.size(); i++) {
			double distance = z.sub(polyRoots.get(i)).module();
			if (distance < minDistance) {
				minDistance = distance;
				result = i + 1;
			}
		}
		return result;
	}
}
