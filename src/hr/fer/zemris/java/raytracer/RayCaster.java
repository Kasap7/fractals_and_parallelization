package hr.fer.zemris.java.raytracer;

import hr.fer.zemris.java.raytracer.model.GraphicalObject;
import hr.fer.zemris.java.raytracer.model.IRayTracerProducer;
import hr.fer.zemris.java.raytracer.model.IRayTracerResultObserver;
import hr.fer.zemris.java.raytracer.model.LightSource;
import hr.fer.zemris.java.raytracer.model.Point3D;
import hr.fer.zemris.java.raytracer.model.Ray;
import hr.fer.zemris.java.raytracer.model.RayIntersection;
import hr.fer.zemris.java.raytracer.model.Scene;
import hr.fer.zemris.java.raytracer.viewer.RayTracerViewer;

/**
 * Program that demonstrates usage of ray-tracer and draws simple picture.
 * 
 * @author Josip Kasap
 *
 */
public class RayCaster {

	/**
	 * Main method for executing RayCaster program
	 * @param args command-line arguments
	 */
	public static void main(String[] args) {
		RayTracerViewer.show(getIRayTracerProducer(),
		new Point3D(10,0,0),
		new Point3D(0,0,0),
		new Point3D(0,0,10),
		20, 20);
	}
	
	/**
	 * Returns {@link IRayTracerProducer} implementation that draws expected picture.
	 * @return {@link IRayTracerProducer} implementation that draws expected picture
	 */
	private static IRayTracerProducer getIRayTracerProducer() {
		return new IRayTracerProducer() {
			@Override
			public void produce(Point3D eye, Point3D view, Point3D viewUp,
					double horizontal, double vertical, int width, int height,
					long requestNo, IRayTracerResultObserver observer) {
				
				System.out.println("Započinjem izračune...");
				short[] red = new short[width*height];
				short[] green = new short[width*height];
				short[] blue = new short[width*height];
				
				Point3D og = view.sub(eye).normalize();
				Point3D yAxis = viewUp.normalize().sub(og.scalarMultiply(og.scalarProduct(viewUp.normalize())));
				yAxis.modifyNormalize();
				Point3D xAxis = og.vectorProduct(yAxis);
				xAxis.modifyNormalize();
				
				Point3D screenCorner = view.add(xAxis.scalarMultiply(horizontal/2).negate().add(
						yAxis.scalarMultiply(vertical/2)));
				
				Scene scene = RayTracerViewer.createPredefinedScene();
				short[] rgb = new short[3];
				int offset = 0;
				for(int y = 0; y < height; y++) {
					for(int x = 0; x < width; x++) {
						Point3D screenPoint = screenCorner.add(xAxis.scalarMultiply((x/(width-1.0))*horizontal)
								.sub(yAxis.scalarMultiply((y/(height-1.0))*vertical)));
						Ray ray = Ray.fromPoints(eye, screenPoint);
						
						tracer(scene, ray, rgb);
						
						red[offset] = rgb[0] > 255 ? 255 : rgb[0];
						green[offset] = rgb[1] > 255 ? 255 : rgb[1];
						blue[offset] = rgb[2] > 255 ? 255 : rgb[2];
						
						offset++;
					}
				}
				
				System.out.println("Izračuni gotovi...");
				observer.acceptResult(red, green, blue, requestNo);
				System.out.println("Dojava gotova...");
			}

			private void tracer(Scene scene, Ray ray, short[] rgb) {
				RayIntersection closest = findClosest(scene, ray);
				
				if (closest == null) {
					rgb[0] = 0;
					rgb[1] = 0;
					rgb[2] = 0;
					return;
				}
				
				rgb[0] = 15; // i.e. ambient component
				rgb[1] = 15;
				rgb[2] = 15;
				
				for (LightSource ls : scene.getLights()) {
					Ray rayFromLight = Ray.fromPoints(ls.getPoint(), closest.getPoint());
					RayIntersection closesFromLight = findClosest(scene, rayFromLight);
					double distanceObjectLight = ls.getPoint().sub(closest.getPoint()).norm();
					double distanceClosestLight;
					
					if (closesFromLight != null) {
						distanceClosestLight = closesFromLight.getDistance();
					} else {
						distanceClosestLight = Double.MAX_VALUE; 
					}
					
					if (compare2Doubles(distanceObjectLight, distanceClosestLight)) {
						Point3D l = rayFromLight.direction.negate();
						Point3D v = ray.direction.negate();
						Point3D n = closest.getNormal();
						
						double nl = l.scalarProduct(n);
						Point3D r = (n.scalarMultiply(2*nl).sub(l)).normalize();
						double rv = r.scalarProduct(v);
						
						nl = nl>0 ? nl : 0;
						rv = rv>0 ? rv : 0;
						
						rgb[0] += ls.getR()*(closest.getKdr()*nl 
								+ closest.getKrr()*Math.pow(rv, closest.getKrn()));
						
						rgb[1] += ls.getG()*(closest.getKdg()*nl 
								+ closest.getKrg()*Math.pow(rv, closest.getKrn()));
						
						rgb[2] += ls.getB()*(closest.getKdb()*nl 
								+ closest.getKrb()*Math.pow(rv, closest.getKrn()));
					}
				}
			}
			
			private RayIntersection findClosest(Scene scene, Ray ray) {
				RayIntersection closest = null;
				for (GraphicalObject obj : scene.getObjects()) {
					RayIntersection intersection = obj.findClosestRayIntersection(ray);
					if (intersection == null) continue;
					
					if (closest == null) {
						closest = intersection;
					} else if (intersection.getDistance() < closest.getDistance()) {
						closest = intersection;
					}
				}
				return closest;
			}
			
			private boolean compare2Doubles(double arg0, double arg1) {
				double distance = arg0 - arg1;
				distance = distance > 0 ? distance : -distance;
				if (distance < 1E-4) return true; // close enough
				return false;
			}
		};
	}
}
