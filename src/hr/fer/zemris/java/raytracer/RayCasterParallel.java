package hr.fer.zemris.java.raytracer;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

import hr.fer.zemris.java.raytracer.model.GraphicalObject;
import hr.fer.zemris.java.raytracer.model.IRayTracerProducer;
import hr.fer.zemris.java.raytracer.model.IRayTracerResultObserver;
import hr.fer.zemris.java.raytracer.model.LightSource;
import hr.fer.zemris.java.raytracer.model.Point3D;
import hr.fer.zemris.java.raytracer.model.Ray;
import hr.fer.zemris.java.raytracer.model.RayIntersection;
import hr.fer.zemris.java.raytracer.model.Scene;
import hr.fer.zemris.java.raytracer.viewer.RayTracerViewer;

/**
 * Program that demonstrates usage of ray-tracer and draws simple picture.
 * This program uses Fork-Join framework with RecursiveAction for parallelization
 * of jobs.
 * 
 * @author Josip Kasap
 *
 */
public class RayCasterParallel {

	/**
	 * Main method for executing the RayCasterParallel program.
	 * @param args command-line arguments
	 */
	public static void main(String[] args) {
		RayTracerViewer.show(getIRayTracerProducer(),
		new Point3D(10,0,0),
		new Point3D(0,0,0),
		new Point3D(0,0,10),
		20, 20);
	}
	
	/**
	 * Returns {@link IRayTracerProducer} implementation that draws expected picture
	 * 		using Fork-Join framework with RecursiveAction for parallelization of jobs.
	 * @return {@link IRayTracerProducer} implementation that draws expected picture
	 * 		using Fork-Join framework with RecursiveAction for parallelization of jobs
	 */
	private static IRayTracerProducer getIRayTracerProducer() {
		return new IRayTracerProducer() {
			@Override
			public void produce(Point3D eye, Point3D view, Point3D viewUp,
					double horizontal, double vertical, int width, int height,
					long requestNo, IRayTracerResultObserver observer) {
				
				System.out.println("Započinjem izračune...");
				short[] red = new short[width*height];
				short[] green = new short[width*height];
				short[] blue = new short[width*height];
				
				Point3D og = view.sub(eye).normalize();
				Point3D yAxis = viewUp.normalize().sub(og.scalarMultiply(og.scalarProduct(viewUp.normalize())));
				yAxis.modifyNormalize();
				Point3D xAxis = og.vectorProduct(yAxis);
				xAxis.modifyNormalize();
				
				Point3D screenCorner = view.add(xAxis.scalarMultiply(horizontal/2).negate().add(
						yAxis.scalarMultiply(vertical/2)));
				
				Scene scene = RayTracerViewer.createPredefinedScene();
				
				ComputingJob job = new ComputingJob(
						scene, eye, screenCorner, yAxis, xAxis, horizontal, vertical, width, height,
						red, green, blue
				);
				
				ForkJoinPool pool = new ForkJoinPool();
				pool.invoke(job);
				pool.shutdown();
				
				System.out.println("Izračuni gotovi...");
				observer.acceptResult(red, green, blue, requestNo);
				System.out.println("Dojava gotova...");
			}
		};
	}
	
	/**
	 * ComputingJob is implementation of RecursiveAction which
	 * recursively calculates resulting image of screen. This
	 * class will calculate result of image if screen is small enough,
	 * if the screen is not small enough, job will be recursively
	 * split into 4 identical screens.
	 * 
	 * @author Josip Kasap
	 *
	 */
	private static class ComputingJob extends RecursiveAction {
		
		@SuppressWarnings("javadoc")
		private static final long serialVersionUID = 1L;
		
		/** Maximum area allowed to be calculated without calling recursive action. */
		public static final int MAX_AREA = 10000;
		
		/** Scene that is to be drawn. */
		private Scene scene;
		
		/** Position of eye. */
		private Point3D eye;
		
		/** Position of screen corner. */
		private Point3D screenCorner;
		
		/** Normalized j vector. */
		private Point3D yAxis;
		
		/** Normalized i vector */
		private Point3D xAxis;
		
		/** Horizontal length. */
		private double horizontal;
		
		/** Vertical length. */
		private double vertical;
		
		/** Width of screen. */
		private int width;
		
		/** Height of screen. */
		private int height;
		
		/** Result field for red color. */
		private short[] red;
		
		/** Result field for green color. */
		private short[] green;
		
		/** Result field for blue color. */
		private short[] blue;
		
		/** Position of first available pixel in y direction. */
		private int yOffset;
		
		/** Position of first available pixel in x direction. */
		private int xOffset;
		
		/** Length available pixels in y direction. */
		private int yLength;
		
		/** Length available pixels in x direction. */
		private int xLength;
		
		/**
		 * Constructs new ComputingJob with given parameters.
		 * This constructor is going to be used for recursive action.
		 * @param scene scene that is to be drawn
		 * @param eye position of eye
		 * @param screenCorner position of screen corner
		 * @param yAxis normalized j vector
		 * @param xAxis normalized i vector
		 * @param horizontal horizontal length
		 * @param vertical vertical length
		 * @param width width of screen
		 * @param height height of screen
		 * @param red result field for red color
		 * @param green result field for green color
		 * @param blue result field for blue color
		 * @param yOffset position of first available pixel in y direction
		 * @param xOffset position of first available pixel in x direction
		 * @param yLength length available pixels in y direction
		 * @param xLength length available pixels in x direction
		 */
		public ComputingJob(Scene scene, Point3D eye, Point3D screenCorner, Point3D yAxis, Point3D xAxis,
				double horizontal, double vertical, int width, int height, short[] red, 
				short[] green, short[] blue, int yOffset, int xOffset, int yLength, int xLength) {
			
			super();
			this.scene = scene;
			this.eye = eye;
			this.screenCorner = screenCorner;
			this.yAxis = yAxis;
			this.xAxis = xAxis;
			this.horizontal = horizontal;
			this.vertical = vertical;
			this.width = width;
			this.height = height;
			this.red = red;
			this.green = green;
			this.blue = blue;
			this.yOffset = yOffset;
			this.xOffset = xOffset;
			this.yLength = yLength;
			this.xLength = xLength;
		}

		/**
		 * Constructs new ComputingJob with given parameters. This constructor is
		 * going to be used for initiating new ComputingJob from other position where
		 * it is needed.
		 * @param scene scene that is to be drawn
		 * @param eye position of eye
		 * @param screenCorner position of screen corner
		 * @param yAxis normalized j vector
		 * @param xAxis normalized i vector
		 * @param horizontal horizontal length
		 * @param vertical vertical length
		 * @param width width of screen
		 * @param height height of screen
		 * @param red result field for red color
		 * @param green result field for green color
		 * @param blue result field for blue color
		 */
		private ComputingJob(Scene scene, Point3D eye, Point3D screenCorner, Point3D yAxis, Point3D xAxis,
				double horizontal, double vertical, int width, int height, short[] red, 
				short[] green, short[] blue) {
			
			super();
			this.scene = scene;
			this.eye = eye;
			this.screenCorner = screenCorner;
			this.yAxis = yAxis;
			this.xAxis = xAxis;
			this.horizontal = horizontal;
			this.vertical = vertical;
			this.width = width;
			this.height = height;
			this.red = red;
			this.green = green;
			this.blue = blue;
			xLength = width;
			yLength = height;
		}
		
		@Override
		protected void compute() {
			int area = yLength * xLength;
			
			if (area < MAX_AREA) {
				computeDirect();
				return;
			} else {
				
				int xLength1 = xLength/2;
				int xOffset2 = xOffset + xLength/2;
				int xLength2 = xLength - xLength/2;
				
				int yLength1 = yLength/2;
				int yOffset2 = yOffset + yLength/2;
				int yLength2 = yLength - yLength/2;
				
				ComputingJob job1 = new ComputingJob(
						scene, eye, screenCorner, yAxis, xAxis, horizontal, vertical, width, height,
						red, green, blue, yOffset, xOffset, yLength1, xLength1
				);
				
				ComputingJob job2 = new ComputingJob(
						scene, eye, screenCorner, yAxis, xAxis, horizontal, vertical, width, height,
						red, green, blue, yOffset, xOffset2, yLength1, xLength2
				);
				
				ComputingJob job3 = new ComputingJob(
						scene, eye, screenCorner, yAxis, xAxis, horizontal, vertical, width, height,
						red, green, blue, yOffset2, xOffset, yLength2, xLength1
				);
				
				ComputingJob job4 = new ComputingJob(
						scene, eye, screenCorner, yAxis, xAxis, horizontal, vertical, width, height,
						red, green, blue, yOffset2, xOffset2, yLength2, xLength2
				);
				
				invokeAll(job1, job2, job3, job4);
			}
		}
		
		/**
		 * Calculates and stores results into given fields of colors.
		 * This method is called when area that is to be calculated is small enough.
		 */
		private void computeDirect() {
			short[] rgb = new short[3];
			
			for(int y = yOffset; y < yOffset + yLength; y++) {
				for(int x = xOffset; x < xOffset + xLength; x++) {
					
					Point3D screenPoint = screenCorner.add(xAxis.scalarMultiply((x/(width-1.0))*horizontal)
							.sub(yAxis.scalarMultiply((y/(height-1.0))*vertical)));
					Ray ray = Ray.fromPoints(eye, screenPoint);
					
					tracer(scene, ray, rgb);
					
					int position = y*width + x;
					red[position] = rgb[0] > 255 ? 255 : rgb[0];
					green[position] = rgb[1] > 255 ? 255 : rgb[1];
					blue[position] = rgb[2] > 255 ? 255 : rgb[2];
				}
			}
		}

		/**
		 * This method stores result of one pixel from one ray. Given
		 * ray must be ray from eye of observer into pixel. If ray
		 * hits some object that object will be drawn, otherwise
		 * result will be (0,0,0).
		 * @param scene scene of objects and lights
		 * @param ray ray from eye of observer into some pixel
		 * @param rgb resulting data of color pixels, 3 for every main
		 * 		color (red, green, blue)
		 */
		private void tracer(Scene scene, Ray ray, short[] rgb) {
			RayIntersection closest = findClosest(scene, ray);
			
			if (closest == null) {
				rgb[0] = 0;
				rgb[1] = 0;
				rgb[2] = 0;
				return;
			}
			
			rgb[0] = 15; // i.e. ambient component
			rgb[1] = 15;
			rgb[2] = 15;
			
			for (LightSource ls : scene.getLights()) {
				Ray rayFromLight = Ray.fromPoints(ls.getPoint(), closest.getPoint());
				RayIntersection closesFromLight = findClosest(scene, rayFromLight);
				double distanceObjectLight = ls.getPoint().sub(closest.getPoint()).norm();
				double distanceClosestLight;
				
				if (closesFromLight != null) {
					distanceClosestLight = closesFromLight.getDistance();
				} else {
					distanceClosestLight = Double.MAX_VALUE; 
				}
				
				if (compare2Doubles(distanceObjectLight, distanceClosestLight)) {
					Point3D l = rayFromLight.direction.negate();
					Point3D v = ray.direction.negate();
					Point3D n = closest.getNormal();
					
					double nl = l.scalarProduct(n);
					Point3D r = (n.scalarMultiply(2*nl).sub(l)).normalize();
					double rv = r.scalarProduct(v);
					
					nl = nl>0 ? nl : 0;
					rv = rv>0 ? rv : 0;
					
					rgb[0] += ls.getR()*(closest.getKdr()*nl 
							+ closest.getKrr()*Math.pow(rv, closest.getKrn()));
					
					rgb[1] += ls.getG()*(closest.getKdg()*nl 
							+ closest.getKrg()*Math.pow(rv, closest.getKrn()));
					
					rgb[2] += ls.getB()*(closest.getKdb()*nl 
							+ closest.getKrb()*Math.pow(rv, closest.getKrn()));
				}
			}
		}
		
		/**
		 * This method finds closes point of intersection and returns result
		 * as implementation of {@link RayIntersection} interface. If there
		 * are not intersection, null will be returned.
		 * @param scene scene of objects and light sources.
		 * @param ray Ray that may hit some object in scene
		 * @return  closes point of intersection if there is some, or <code>null</code>
		 * 			otherwise
		 */
		private RayIntersection findClosest(Scene scene, Ray ray) {
			RayIntersection closest = null;
			for (GraphicalObject obj : scene.getObjects()) {
				RayIntersection intersection = obj.findClosestRayIntersection(ray);
				if (intersection == null) continue;
				
				if (closest == null) {
					closest = intersection;
				} else if (intersection.getDistance() < closest.getDistance()) {
					closest = intersection;
				}
			}
			return closest;
		}
		
		/**
		 * Helping method that compare 2 double numbers.
		 * @param arg0 first double number
		 * @param arg1 second double number
		 * @return <code>true</code> if 2 numbers are very close to each other
		 * 		(equal), <code>false</code> otherwise
		 */
		private boolean compare2Doubles(double arg0, double arg1) {
			double distance = arg0 - arg1;
			distance = distance > 0 ? distance : -distance;
			if (distance < 1E-4) return true; // close enough
			return false;
		}
	}
}
