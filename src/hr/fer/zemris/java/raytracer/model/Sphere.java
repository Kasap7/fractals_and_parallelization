package hr.fer.zemris.java.raytracer.model;

/**
 * Sphere is implementation of {@link GraphicalObject} which is determined
 * by center point and radius. In layman's terms, it is bowl with some 
 * diameter in some position.
 * 
 * @author Josip Kasap
 *
 */
public class Sphere extends GraphicalObject {
	
	/** Center of sphere. */
	private Point3D center;
	
	/** Radius of sphere. */
	private double radius;
	
	/** Coefficient for diffuse component for red color */
	private double kdr;
	
	/** Coefficient for diffuse component for green color */
	private double kdg;
	
	/** Coefficient for diffuse component for blue color */
	private double kdb;
	
	/** Coefficient for reflective component for red color */
	private double krr;
	
	/** Coefficient for reflective component for green color */
	private double krg;
	
	/** Coefficient for reflective component for blue color */
	private double krb;
	
	/** Coefficient <code>n</code> for reflective component. */
	private double krn;
	
	/**
	 * Constructs new sphere with given center, radius and coefficients.
	 * @param center center of sphere
	 * @param radius radius of sphere
	 * @param kdr coefficient for diffuse component for red color
	 * @param kdg coefficient for diffuse component for green color
	 * @param kdb coefficient for diffuse component for blue color
	 * @param krr coefficient for reflective component for red color
	 * @param krg coefficient for reflective component for green color
	 * @param krb coefficient for reflective component for blue color
	 * @param krn coefficient <code>n</code> for reflective component
	 */
	public Sphere(Point3D center, double radius, double kdr, double kdg,
			double kdb, double krr, double krg, double krb, double krn) {
		
		super();
		
		if (center == null) {
			throw new IllegalArgumentException("Center cannot be null value.");
		}
		this.center = center;
		this.radius = radius;
		this.kdr = kdr;
		this.kdg = kdg;
		this.kdb = kdb;
		this.krr = krr;
		this.krg = krg;
		this.krb = krb;
		this.krn = krn;
	}

	@Override
	public RayIntersection findClosestRayIntersection(Ray ray) {
		
		double a = ray.direction.x*ray.direction.x + ray.direction.y*ray.direction.y 
				+ ray.direction.z*ray.direction.z;
		
		double b = 2*(ray.start.x-center.x)*ray.direction.x 
				+ 2*(ray.start.y-center.y)*ray.direction.y
				+ 2*(ray.start.z-center.z)*ray.direction.z;
		
		double c = (ray.start.x-center.x)*(ray.start.x-center.x)
				+ (ray.start.y-center.y)*(ray.start.y-center.y)
				+ (ray.start.z-center.z)*(ray.start.z-center.z)
				- radius*radius;
		//a*€^2 + b*€ + c = 0; točka presjeka = ray.start + €*ray.direction;
		
		double disc = b*b - 4*a*c;
		
		if (disc < 1E-9) return null;
		
		double lambda1 = (-b + Math.sqrt(b*b-4*a*c))/2;
		double lambda2 = (-b - Math.sqrt(b*b-4*a*c))/2;
		Point3D point1;
		Point3D point2;
		double distance1;
		double distance2;
		
		if (lambda1 > 0) {
			point1 = ray.start.add(ray.direction.scalarMultiply(lambda1));
			distance1 = point1.sub(ray.start).norm();
		} else {
			distance1 = Double.MAX_VALUE;
			point1 = null;
		}
		
		if (lambda2 > 0) {
			point2 = ray.start.add(ray.direction.scalarMultiply(lambda2));
			distance2 = point2.sub(ray.start).norm();
		} else {
			distance2 = Double.MAX_VALUE;
			point2 = null;
		}
		
		Point3D point;
		double distance;
		
		if (distance1 < distance2) {
			point = point1;
			distance = distance1;
		} else {
			point = point2;
			distance = distance2;
		}
		
		if (point != null) {
			return new SphereRayIntersectionImpl(point, distance, true);
		} else {
			return null;
		}
	}
	
	/**
	 * SphereRayIntersectionImpl is implementation of {@link RayIntersection}
	 * for sphere.
	 * 
	 * @author Josip Kasap
	 *
	 */
	private class SphereRayIntersectionImpl extends RayIntersection {
		
		/** Normal vector, normalized vector from center, to intersection point. */
		private Point3D normalVector;

		/**
		 * Constructs new SphereRayIntersectionImpl with given parameters.
		 * @param point point of intersection
		 * @param distance distance from intersection point and source of ray
		 * @param outer <code>true</code> if intersection is outer, <code>false</code>
		 * 			otherwise
		 */
		public SphereRayIntersectionImpl(Point3D point, double distance, boolean outer) {
			super(point, distance, outer);
			normalVector = point.sub(center).normalize();
		}

		@Override
		public Point3D getNormal() {
			return normalVector;
		}

		@Override
		public double getKdr() {
			return kdr;
		}

		@Override
		public double getKdg() {
			return kdg;
		}

		@Override
		public double getKdb() {
			return kdb;
		}

		@Override
		public double getKrr() {
			return krr;
		}

		@Override
		public double getKrg() {
			return krg;
		}

		@Override
		public double getKrb() {
			return krb;
		}

		@Override
		public double getKrn() {
			return krn;
		}
	}
}
